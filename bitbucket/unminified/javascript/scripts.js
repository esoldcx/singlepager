$(document).ready(function(){
    $('.big-m').each(function(){
        $(this).monashM({
            'sides': true,
            'style': $(this).attr('data-style')
        });
    });
});


(function($){
    
    var zz = $(document);
    
    $.fn.monashM = function(options){
        
        var settings = $.extend({
            "style": "black",
            "animation": null,
            "sides": false
        },options),
            
            target = this,
            
            ratio = 1.8,

            monashM = $('<div/>').attr('class','monash-m absolute top bottom transition'),
            topWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--top-wedge background-image absolute top'),
            bottomLeftWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--bottom-left-wedge background-image absolute bottom'),
            bottomRightWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--bottom-right-wedge background-image absolute bottom'),
        
            height = target.outerHeight(),
            width = height/ratio,
            
            items = [];
        
        if(width>=$(window).width()*.325){
            width = $(window).width()*.325;
        }
        
        if(!target.hasClass('big-m-loaded')){
            
            target.addClass('big-m-loaded');
        
            monashM.css('width',width);

            monashM.append(
                topWedge
            ).append(
                bottomLeftWedge
            ).append(
                bottomRightWedge
            );

            target.append(monashM);

            items.push(topWedge,bottomLeftWedge,bottomRightWedge);

            if(settings.sides===true){

                var targetWidth = target.outerWidth(),
                    masterWrap = $('<div/>').attr('class','master-wrap');

                target.append(masterWrap);

                var masterWrapWidth = masterWrap.outerWidth(),
                    leftWidth = (targetWidth-masterWrapWidth)/2,
                    mWidth = width,
                    rightWidth = (targetWidth-width)-leftWidth,
                    rightOffset = width+leftWidth,
                    contentOffset = width;
                /*
                if($(window).width()<=720){
                    leftWidth = '30%';
                    rightWidth = '30%';
                    mWidth = '40%';
                    rightOffset = '70%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=609){
                    leftWidth = '20%';
                    rightWidth = '20%';
                    mWidth = '60%';
                    rightOffset = '80%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=480){
                    leftWidth = '12%';
                    rightWidth = '12%';
                    mWidth = '76%';
                    rightOffset = '88%';
                    contentOffset = '0%';
                }
                */
                
                monashM.css('width',mWidth);

                var leftBlock = $('<div/>').attr('class',settings.style+'-background absolute top bottom transition').css('width',leftWidth),
                    rightBlock = $('<div/>').attr('class',settings.style+'-background absolute top right bottom transition').css({
                        // 'width': rightWidth,
                        'left': rightOffset
                    });

                monashM.css('left',leftWidth);

                target.prepend(leftBlock);
                target.append(rightBlock);

                leftBlock.css({
                    'opacity': 0,
                    'transform': 'translate3d(-700px,0px,0px)'
                });

                rightBlock.css({
                    'opacity': 0,
                    'transform': 'translate3d(700px,0px,0px)'
                });

                items.push(leftBlock,rightBlock);

                var container = target.closest('.big-m-container');
                if(container.length>0){

                    var content = container.find('.big-m-offset');
                    content.css('margin-left',contentOffset);
                }

            }

            topWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,-700px,0px)'
            });
            bottomLeftWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,700px,0px)'
            });
            bottomRightWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,700px,0px)'
            });

            setTimeout(function(){
                for(var i = 0; i < items.length; i++){
                    for(var i = 0; i < items.length; i++){
                        $(items[i]).addClass('long-transition');
                    }
                }
            },100);

            setTimeout(function(){
                for(var i = 0; i < items.length; i++){
                    $(items[i]).css({
                        'opacity': 1,
                        'transform': 'translate3d(0px,0px,0px)'
                    });
                }
            },200);

            $(window).resize(function(){

                for(var i = 0; i < items.length; i++){
                    $(items[i]).removeClass('long-transition');
                }

                height = target.outerHeight(),
                width = height/ratio;
                
                if(width>=$(window).width()*.325){
                    width = $(window).width()*.325;
                }
                
                mWidth = width;
                targetWidth = target.outerWidth();
                masterWrapWidth = masterWrap.outerWidth();
                leftWidth = (targetWidth-masterWrapWidth)/2;
                rightWidth = (targetWidth-width)-leftWidth;
                rightOffset = width+leftWidth;
                contentOffset = width;
/*
                if($(window).width()<=720){
                    leftWidth = '30%';
                    rightWidth = '30%';
                    mWidth = '40%';
                    rightOffset = '70%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=609){
                    leftWidth = '20%';
                    rightWidth = '20%';
                    mWidth = '60%';
                    rightOffset = '80%';
                    contentOffset = '0%';
                }

                if($(window).width()<=480){
                    leftWidth = '12%';
                    rightWidth = '12%';
                    mWidth = '76%';
                    rightOffset = '88%';
                    contentOffset = '0%';
                }
*/
                content.css('margin-left',contentOffset);

                monashM.css({
                    'width': mWidth,
                    'left': leftWidth
                });

                leftBlock.css('width',leftWidth);
                rightBlock.css({
                //    'width': rightWidth,
                    'left': rightOffset
                });
            });
            
        }
        
    };
    
})(jQuery);
$(document).ready(function(){
    
    $('.one-pager-wrapper').each(function(){
        var target = $(this);
        
        target.onePager('init');
        
        $(window).scroll();
        
        $(window).resize(function(){
            target.onePager('update');
        });
    });
    
    $('body').on('click','ul.one-pager-menu > li > a, .one-pager-editor-navigator > li > .listed-page-thumbnail',function(e){
        var item = $(this),
            index = item.parent('li').index(),
            pages = $('.one-pager-wrapper').children('.asset'),
            page = $(pages[index]),
            scroll = page.offset().top-$('header').outerHeight();
        
        $('html, body').stop(true,true).animate({
            scrollTop: scroll
        },1000,'easeOutQuart');
        
        e.preventDefault();
    });
    
    $('body').on('click','a.one-pager-next-link',function(e){
        var item = $(this).closest('.asset'),
            index = item.index()+1,
            pages = $('.one-pager-wrapper').children('.asset'),
            page = $(pages[index]),
            scroll = page.offset().top-$('header').outerHeight();
        
        $('html, body').stop(true,true).animate({
            scrollTop: scroll
        },1000,'easeOutQuart');
        
        e.preventDefault();
    });
    
});


(function($){
    
    var zz = $('<i/>'),
    
    // Non-repeating delay function
    typeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();

    function closestNumber(array,num){
        var i=0;
        var minDiff=1000;
        var ans;
        for(i in array){
             var m=Math.abs(num-array[i]);
             if(m<minDiff){ 
                    minDiff=m; 
                    ans=array[i]; 
                }
          }
        return ans;
    }
    
    $.fn.onePager = function(command){
        var target = $(this),
            pages,
            menuStyle;
        
        if(zz.checkVariable(target)){

            pages = target.children('.asset');
            
            menuStyle = target.attr('data-menu-style');
            
            if(zz.checkVariable(pages)){
                
                // INITIATE
                if(command==='init'){
                    
                    target.find('.generated-ui').remove();
                    $('.sticky-nav-tracks.one-pager-nav').empty();
                    
                    var asset = target.attr('data-asset'),
                        layout = target.attr('data-result-layout'),
                        nextTab = zz.element('div','absolute right bottom left large-padding center only-if-active'),
                        nextLink = zz.element('a','one-pager-next-link circle inline-block larger white no-underline').attr('href','#'),
                        nextIcon = zz.icon('angle-down'),
                        
                        // Menu elements
                        
                        pagesMenuWrap = zz.element('div','page-menu-wrap helper overflow-initial'),
                        pagesMenu = zz.element('ul','no-list list-menu no-wrap one-pager-menu').attr({
                            'role': 'tablist',
                            'data-asset': asset,
                            'data-result-layout': layout
                        }),
                        pagesMenuOptions = [];

                    nextTab.append(
                        nextLink.append(
                            nextIcon
                        )
                    );
                    
                    // Page generation

                    for(var i = 0; i < pages.length; i++){
                        
                        // Next page button
                        
                        if($(pages[i]).hasClass('window-height')){
                            $(pages[i]).matchWindowHeight();
                        }
                        
                        var thisNextTab = nextTab.clone();
                        
                        if(i<pages.length-1){
                        //    $(pages[i]).append(thisNextTab);
                        //    thisNextTab.introduce(1000,'bottom');
                        }
                        
                        // Menu links
                        
                        var count = i+1,
                            
                            name = $(pages[i]).attr('data-name'),
                            id = $(pages[i]).attr('data-asset'),
                            anchor = zz.element('a','block large no-underline short-transition relative').attr({
                                'id': 'page-tab-'+count,
                                'href': '#',
                                'role': 'tab',
                                'aria-controls': 'page-section-'+count
                            }),
                            icon = '', //zz.icon('caret-right'),
                            label = zz.element('span').attr({
                                "data-editable":"true",
                                "data-attribute":"short_name",
                                "data-save-to": id
                            }).html(name),
                            marker = zz.element('span','menu-active-item-marker absolute bottom right left margin-auto transition width-0'),
                            item = zz.element('li','list-menu-item').attr('data-asset',id).append(
                                anchor.append(
                                    icon
                                ).append(
                                    label
                                ).append(
                                    marker
                                )
                            );
                        
                        $(pages[i]).attr({
                            'id': 'page-section-'+count,
                            'aria-labelledby': 'page-tab-'+count,
                            'role': 'tabpanel'
                        });
                        
                        pagesMenuOptions.push(item);
                        
                        if(i===0){
                            // anchor.addClass('active untrim white-background');
                        }
                        
                        var nextPageLink = $(pages[i]).find('a.one-pager-next-link'),
                            nextPage = $(pages[i+1]);
                        
                        if(zz.checkVariable(nextPage)){
                            
                            var nextPageLabel = nextPage.attr('data-name'),
                                nextPageID = nextPage.attr('id');
                            
                            nextPageLink.children('span').html(nextPageLabel);
                            nextPageLink.show();
                            
                            nextPageLink.attr({
                                'aria-controls': 'page-section-'+(i+2)
                            });
                        }else{
                            nextPageLink.hide();
                        }
                    }
                    
                    pagesMenuWrap.append(
                        pagesMenu.append(
                            pagesMenuOptions
                        )
                    );
                    
                    if(menuStyle==="Always on Top"){
                        pagesMenuWrap.addClass('relative top').appendTo('.sticky-nav-tracks.one-pager-nav');
                        pagesMenu.children('li');
                    }
                    
                    var heightBuffer = zz.element('div','sticky-nav-height-buffer no-desktop relative top-layer white-background').css('height',pagesMenuWrap.outerHeight()-1);
                    // $(pages[0]).prepend(heightBuffer);
                    
                    var stuck = 0,
                        swiping = 1;
                    
                    $(window).scroll(function(){
                        
                        typeDelay(function(){
                            
                            var winScroll = $(window).scrollTop()+$('.header-height-buffer').outerHeight(),
                                offsets = [],
                                elements = [],
                                divs = {};
                            
                            if(winScroll>150){
                                $('.sticky-nav-tracks.one-pager-nav').addClass('opaque');
                            }else{
                                $('.sticky-nav-tracks.one-pager-nav').removeClass('opaque');
                            }

                            for(var i = 0; i < pages.length; i++){
                                var target = $(pages[i]),
                                    thisScroll = target.offset().top,
                                    thisLabel = target.attr('data-name'),
                                    alreadyActive = target.attr('data-slide-active');

                                offsets.push(thisScroll);
                                elements.push(target);
                                
                                divs[thisScroll] = {
                                    "label": thisLabel,
                                    "alreadyActive": alreadyActive,
                                    "element": target,
                                    "index": i
                                };

                            }

                            var closestOffset = closestNumber(offsets,winScroll),
                                closestElement = divs[closestOffset].label,
                                alreadyActive = divs[closestOffset].alreadyActive;

                            if(alreadyActive!=="1"){

                                for(var i = 0; i < pages.length; i++){
                                    $(pages[i]).attr({
                                        'data-slide-active':'0',
                                        'aria-hidden': 'true'
                                    });
                                    
                                    $(pages[i]).find('video.outline-shadow').each(function(){
                                        var vid = $(this);
                                        vid[0].pause();
                                    });
                                }
                                
                                pagesMenuWrap.swipeMenu(swiping,divs[closestOffset].index);
                                
                                // Highlight current item in editor nav (here to save multiple scroll listeners)
                                
                                if($('.one-pager-editor-navigator').length>0){
                                    
                                    var highlightThumb = $('.one-pager-editor-navigator').children('li').eq(divs[closestOffset].index),
                                        highlightThumbMarker = highlightThumb.find('.asset-marker');
                                    
                                    $('.one-pager-editor-navigator').find('.asset-marker').removeClass('opaque');
                                    highlightThumbMarker.addClass('opaque');
                                    
                                    scrollParent = highlightThumb.closest('.overflow-auto');
                                    
                                    // var scrollOffset = (highlightThumb.position().top-scrollParent.outerHeight(true))+(highlightThumb.outerHeight(true)*3.5);
                                    var scrollOffset = (highlightThumb.position().top)-15;
                                    
                                    console.log(scrollOffset);
                                    
                                    if(divs[closestOffset].index===0){
                                        scrollOffset = 0;
                                    }
                                    
                                    scrollParent.stop(true,true).animate({
                                        scrollTop: scrollOffset
                                    },500,'easeOutExpo');
                                }
                                
                                divs[closestOffset].element.find('video.outline-shadow').each(function(){
                                    var vid = $(this);
                                    vid[0].play();
                                });
                                
                                divs[closestOffset].element.attr({
                                    'data-slide-active':'1',
                                    'aria-hidden': 'false'
                                });
                            }
                            
                        },100);
                    });
                }
                    
                if(swiping===1){
                    swiping = pagesMenuWrap.swipeMenu(null,null,1);
                }
                
                // UPDATE
                if(command==='update'){
                    for(var i = 0; i < pages.length; i++){
                        
                        if($(pages[i]).hasClass('window-height')){
                            $(pages[i]).css('margin-top','-1px').matchWindowHeight();
                        }
                    }
                }                
            }
        }
    };

})(jQuery);
;
$(document).ready(function(){


    if (/MSIE 10/i.test(navigator.userAgent)) {
       // This is internet explorer 10
       ieStyles();
    }

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
        // This is internet explorer 9 or 11
       ieStyles();
    }

    if (/Edge\/\d./i.test(navigator.userAgent)){
       // This is Microsoft Edge
       ieStyles();
    }
    
    function ieStyles(){
        var ieCSS = '<link href="https://www.monash.edu/__data/assets/css_file/0014/531023/inernet-explorer.css" rel="stylesheet" type="text/css" media="all"/>';
        $('head').append(ieCSS);
    }
    
    // Skip to content
    $('.skip-to-content').click(function(e){
        var page = $('.page-container');
        page.attr('tabindex','-1').focus();
        e.preventDefault();
    });
    
    $('.page-container').blur(function(){
        $(this).removeAttr('tabindex'); 
    });

    // Non-repeating delay function
    typeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();
    
    // Only run slideContentIn if we're not in an iframe
    if(window.top!=window.self){
        
        $('.load-in-ani > *').css({
            'opacity': '1'
        });
        
    }else{
        $('body').slideContentIn(350,200,null,1);

        $(window,parent.window).scroll(function(){
            
            if($('.sly-container').length<1){
                typeDelay(function(){
                    $('body').slideContentIn(100,125); 
                },10);
            }
        });
    }
    
    $('body').middleOfParent();
    $('body').adjustGalleries();
    $('body').scrollAssist();
    $('body').stickToHeader();
    $('body').backgroundVideoControls();
    $('body').iconPattern();
    
    // Issue fixes
    
    /*
        Issue where mobile background image fixed jumps on screen resize
    */
    
    var origHeight = $(window).height(),
        origWidth = $(window).width();
    
    $(window).resize(function(){
        
        $('body').middleOfParent();
        $('body').adjustGalleries();
        
        
        var fixedBackgrounds = $('.background-image.fixed.top');
        
        if($(window).width()<=1024){

            var newHeight = $(window).height(),
                newWidth = $(window).width();

            // Scroll Down and hide toolbar
            if(origHeight!==newHeight && origWidth===newWidth){
                fixedBackgrounds.css({
                    'top':-60+(newHeight-origHeight),
                    'bottom': origHeight
                });
            }

            // Scroll Up and show toolbar
            if(origHeight===newHeight && origWidth===newWidth){
                fixedBackgrounds.css({
                    'top':-60,
                    'height': origHeight+60
                });
            }
            
            // Likely resize on desktop, or rotating device
            if(origHeight!==newHeight && origWidth!==newWidth){
                fixedBackgrounds.css({
                    'top': 0,
                    'height': newHeight
                });
                origHeight = newHeight;
                origWidth = newWidth;
            }
        }
        
    });
    
    $('body').on('click','a.video-ui-toggle',function(e){
        
        var thisVideo = $(this).closest('.asset').find('video.background-video');
                 
        if($(this).hasClass('playing')){
            thisVideo[0].pause();
            $(this).removeClass('playing');
            $(this).addClass('paused');
        }else{
            thisVideo[0].play();
            $(this).addClass('playing');
            $(this).removeClass('paused');
        }
        e.preventDefault();
    });
    
    
    var slys = $('.standard-horizontal-sly');
    
    /*
    for(var i = 0; i < slys.length; i++){
        $(slys[i]).swipeMenu();
    } */
    
    $(slys).each(function(){
        $(this).swipeMenu();
    });
    
    // Start behavs
    
    $('body').progressiveLoad();
    
    // Find infographic elements and run them
    var infographics = $('.run-infographic');

    for(var i = 0; i < infographics.length; i++){
        var $target = $(infographics[i]),
            type = $target.children('.infographic-type').val(),
            dataX = $target.children('.infographic-data-x').val(),
            dataY = $target.children('.infographic-data-y').val(),
            dataZ = $target.children('.infographic-data-Z').val(),
            dataArray = $target.children('.infographic-array').val();

        if(!type||type===""||type===null){}else{

            $target.graph({
                type: type,
                dataX: dataX,
                dataY: dataY,
                dataZ: dataZ,
                dataArray: dataArray
            });
        }
    }
    
    $('body').on('click','a.play-video-button, div.play-video',function(e){
        
        var target = $(this),
            container = target.closest('.video-container'),
            video = container.children('iframe'),
            overlay = container.children('.play-video'),
            src = video.attr('src')+'&autoplay=1';
        
        overlay.remove();

        video.attr('src',src);
        
        e.preventDefault();
        
        return false;
    });
    
    
    var tabs = $('ul.tabs');
    for(var i = 0; i < tabs.length; i++){
        $(tabs[i]).tabList();
    }
    
    // Trigger resize to make any adjustments post initial CSS load
    $(window).resize();
    
});

(function($){
    
    var zz = $('<i/>'),
    
    // Non-repeating delay function
    scrollTypeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();
    
    var scrollTop = 0,
        threshold = 30;

    $(window).scroll(function(){
        
        var thisScroll = 0;
        
        scrollTypeDelay(function(){
            
            thisScroll = $(window).scrollTop();

            if(thisScroll<=threshold){

                adjustStickyHeader(0);

            }else{

                if(thisScroll>scrollTop){
                    if(thisScroll-scrollTop>threshold){
                        adjustStickyHeader(thisScroll,'down');
                    }
                }else{
                    if(scrollTop-thisScroll>threshold){
                        adjustStickyHeader(thisScroll,'up');
                    }
                }

            }
            
            
            scrollTop = thisScroll;
            
        },15);

        
        $('body').progressiveLoad();
    });
    
    function adjustStickyHeader(scrollTop,direction){

        var item = $('header'),
            itemHeight = item.outerHeight()+1;
        
        if(scrollTop<1){
            
            item.css({
                'transform': 'translate3d(0px,0px,0px)',
                '-webkit-transform': 'translate3d(0px,0px,0px)',
                '-moz-transform': 'translate3d(0px,0px,0px)',
                '-ms-transform': 'translate3d(0px,0px,0px)'
            });
            
        }else{
            if(scrollTop>=itemHeight){
                
                item.css({
                    'transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                    '-webkit-transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                    '-moz-transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                    '-ms-transform': 'translate3d(0px,-'+itemHeight+'px,0px)'
                });
                
                if(direction==='up'){
                    item.css({
                        'transform': 'translate3d(0px,0px,0px)',
                        '-webkit-transform': 'translate3d(0px,0px,0px)',
                        '-moz-transform': 'translate3d(0px,0px,0px)',
                        '-ms-transform': 'translate3d(0px,0px,0px)'
                    });
                }else{
                    
                    item.css({
                        'transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                        '-webkit-transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                        '-moz-transform': 'translate3d(0px,-'+itemHeight+'px,0px)',
                        '-ms-transform': 'translate3d(0px,-'+itemHeight+'px,0px)'
                    })
                }
            }else{
            }
        }
    };
    
    $.fn.scrollAssist = function(){
        
        var target = $(this),
            ui = target.find('.return-to-top-navigation');
        
        if(zz.checkVariable(ui)){
            
        }else{
            
            ui = $('<div/>').attr('class','return-to-top-navigation fixed bottom right small-padding transition');
            
            var button = $('<a/>').attr('class','inline-block default-trim small-round-corners padding go-to-top center').attr({
                    'href':'#',
                    'title': 'Go to top'
                }),
                icon = zz.icon('caret-up',null,'small-padding');
            
            target.append(
                ui.append(
                    button.append(
                        icon
                    )
                )
            );
            
            button.click(function(e){
                $('html, body').stop(true,true).animate({
                    scrollTop: 0
                },1000,'easeOutQuart');
                
                e.preventDefault();
            });
            
            $(window).scroll(function(){
                var winHeight = $(window).height(),
                    scroll = $(window).scrollTop();

                if(scroll>=winHeight*1){
                    $('body').addClass('scrolled-down');
                }else{
                    $('body').removeClass('scrolled-down');
                }
            });
        }
    };
    
    // Check Variables
    
    $.fn.checkVariable = function(check){

        if(!check||check===""||check==="undefined"||check===null||check==="null"||check.length<1){
            return false;
        }else{
            return true;
        }
    };
    
    
    
    // Get a new HTML element
    
    $.fn.element = function(elementType,classes){
        var element = $('<'+elementType+'/>').attr('class',"generated-ui "+classes);
        return element;
    };
    
    
    
    // Get an icon
    
    $.fn.icon = function(name,family,classes){
        if(zz.checkVariable(family)){}else{
            // Default to Font Awesome
            family = "fa fa-";
        }
        if(zz.checkVariable(classes)){}else{
            // Default to Font Awesome
            classes = "";
        }
        
        return $('<i/>').attr('class',family+name+" "+classes);
    };
    
    
    
    // Animate element in via CSS
    
    $.fn.introduce = function(delay,anchorPoint){
        
        var element = $(this);
        
        if(zz.checkVariable(delay)){}else{
            delay = 500;
        }
        
        if(zz.checkVariable(anchorPoint)){}else{
            anchorPoint = 'top'
        }
        
        var offset = 20;
        
        if(anchorPoint==='top'){
            offset = 0-offset;
        }
        
        element.css(anchorPoint,offset+'em');
        element.css('opacity',0);
        element.show();
        
        setTimeout(function(){
            element.addClass('short-transition');
            element.removeAttr('style');
        },delay);
    };
    
    
    // Set to same height as window, minus fixed header
    
    $.fn.matchWindowHeight = function(){
        var winHeight = $(window).height()-$('header').outerHeight();
        $(this).css('height',winHeight);
    };
    
    $.fn.backgroundVideoControls = function(){
        var target = $(this),
            backgroundVideos = target.find('video.background-video');
        
        for(var i = 0; i < backgroundVideos.length; i++){
            
            var thisVideo = $(backgroundVideos[i]),
                videoID = $(thisVideo).attr('id');
                
            if(thisVideo.hasClass('ui-added')){}else{
                
                var uiContainer = $('<div/>').attr('class','absolute bottom right top-layer desktop-only'),
                    uiInner = $('<div/>').attr('class','padding'),
                    uiToggle = $('<a/>').attr('class','video-ui-toggle white pointer playing large text-shadow').attr({
                        'href': '#',
                        'title': 'Play/Pause background video',
                        'aria-controls': videoID
                    }),
                    uiIcons = [
                        zz.icon('play',null,'play-video-icon'),
                        zz.icon('pause',null,'pause-video-icon')
                    ];
                
                var parentAssetContainer = $(thisVideo).closest('.asset');
                
                parentAssetContainer.addClass('layer-1').append(
                    uiContainer.append(
                        uiInner.append(
                            uiToggle.append(
                                uiIcons
                            )
                        )
                    )
                );

                thisVideo.addClass('ui-added');
            }
        }
    };
    
    $.fn.progressiveLoad = function(){
        
        var target = this,
            items = target.find('.background-image,.background-video'),
            winHeight = $(window).height()-$('header').outerHeight(),
            winScroll = $(window).scrollTop(),
            load = false;
        
        for(var i = 0; i < items.length; i++){

            var $target = $(items[i]),
                scrollY = $target.offset().top;
            
            if($target.hasClass('fixed')){
                scrollY = $target.closest('.relative').offset().top;
            }
            
            if(scrollY<(winHeight*2)+winScroll&&!$target.hasClass('loaded')){
                
                if($target.is('div')){
                    var style = $target.attr('data-background');
                    $target.attr('style',style);
                }

                if($target.is('video')){
                    $target[0].play();
                    console.log("Video loaded");
                }
                
                $target.addClass('loaded');
            }
        }
    };
    
    // Makes a horizontal list menu swipable
    
    $.fn.swipeMenu = function(sly,index,reset){
        
        if(!index){
            index = 0;
        }
        
        var $frame = $(this),
            $parent = $frame.parent(),
            $links = $frame.find('a'),
            $tabbable = $frame.find('*[tabindex]'),
            slides = $frame.children('ul').children('li'),
            helper = zz.element('div','absolute top right bottom left no-click swipe-menu-helper'),
            options = {
                smart: true,
                itemNav: 'basic',
                startAt: 0,
                speed: 300,
                easing: 'easeOutExpo',
                swingSpeed: 0.2,
                horizontal: true,
                scrollSource: $parent,
                scrollBy: 0,
                scrollBar: $parent.find('.scrollbar'),
                activateOn: 'click',
                activatePageOn: 'click',
                elasticBounds: 1,
                touchDragging: 1,
                releaseSwing: 1,
                mouseDragging: 1,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                interactive: '[data-editable][data-field]',
                prev: $parent.closest('.asset').find('a.sly-prev-item'),
                next: $parent.closest('.asset').find('a.sly-next-item'),
                prevPage: $parent.closest('.asset').find('a.sly-prev'),
                nextPage: $parent.closest('.asset').find('a.sly-next'),
                pagesBar: $parent.closest('.asset').find('.pages-nav'),
                pageBuilder: function(index){
                    
                    var pageTab = '<li class="inline-block pointer" aria-label="Go to item '+(index+1)+'"></li>';
                    return pageTab;
                }
            };
        
        if($parent.children('.swipe-menu-helper').length<1&&$frame.hasClass('helper')){
            $parent.append(helper);
        }
        
        if(!$frame.attr('data-interval')){}else{
            options['speed'] = 1000;
            options['cycleBy'] = 'items';
            options['cycleInterval'] = $frame.attr('data-interval')+'000';
            options['pauseOnHover'] = true;
        }
        
        if($frame.hasClass('full-width-sly')){
            
            for(var i = 0; i < slides.length; i++){

                $(slides[i]).css({
                    'width':$frame.width()
                });
                
            }
            
        }else{
        
            for(var i = 0; i < slides.length; i++){

                $(slides[i]).css({
                    'width':'auto',
                    'height': 'auto'
                });
                var width = $(slides[i]).outerWidth();

             //   $(slides[i]).css('width',width);
            }
            
        }
        
        if(!sly){
            
            sly = new Sly($frame,options);
            
            sly.on('moveStart',function(){
            });
            
            sly.on('moveEnd',function(){
            });
            
            sly.on('cycle',function(){
            });
            
            sly.on('activate',function(){
                console.log("activate");
            });
            
            $links.attr('aria-selected','false');
            
            slides.focus(function(e){
                var target = $(this),
                    link = target.find('a').eq(0);
                
                link.focus();
                console.log("Slide focus");
                e.preventDefault();
            });
            
            $tabbable.focus(function(){
                var target = $(this),
                    slide = target.closest('li'),
                    index = slide.index();
                
                $frame.closest('.transparent').addClass('opaque');

                sly.activate(index);
            });

            $links.focus(function(e){

                var target = $(this),
                    slide = target.closest('li'),
                    index = slide.index();
                
                $frame.closest('.transparent').addClass('opaque');

                sly.activate(index);
                
                // e.preventDefault();
            });
            
            // Keyboard navigation for tabs
            $links.keyup(function(e){
                
                var keyCode = (event.keyCode ? event.keyCode : event.which);
                
                if(keyCode==13){
                    var target = $(this),
                        relation = target.attr('aria-controls');

                    if(zz.checkVariable(relation)){
                        $('#'+relation).attr("tabindex",-1).focus();
                    }
                    
                    e.preventDefault();
                }
            });

            sly.init();

            $(window).resize(function(){
                if($frame.hasClass('full-width-sly')){

                    for(var i = 0; i < slides.length; i++){

                        $(slides[i]).css({
                            'width':$frame.width()
                        });

                    }

                }else{

                    for(var i = 0; i < slides.length; i++){

                        $(slides[i]).css({
                            'width':'auto',
                            'height': 'auto'
                        });
                        var width = $(slides[i]).outerWidth();

                     //   $(slides[i]).css('width',width);
                    }

                }

                sly.reload();
                
                if($($frame).closest('.master-wrap').width()<$frame.children('ul').outerWidth()&&!$frame.hasClass('standard-horizontal-sly')){
                    helper.show();
                    $($frame).closest('.master-wrap').addClass('initial');
                }else{
                    helper.hide();
                    $($frame).closest('.master-wrap').removeClass('initial');
                }
            });
            
            if($($frame).closest('.master-wrap').width()<$frame.children('ul').outerWidth()&&!$frame.hasClass('standard-horizontal-sly')){
                helper.show();
                $($frame).closest('.master-wrap').addClass('initial');
            }else{
                helper.hide();
                $($frame).closest('.master-wrap').removeClass('initial');
            }
            
            return sly;
            
        }else{
            sly.activate(index);
            slides.find('a').attr('aria-selected','false');
            slides.eq(index).find('a').attr('aria-selected','true');
            
            /* if($frame.hasClass('full-width-sly')){
                slides.attr('aria-hidden','false');
                slides.eq(index).attr('aria-hidden','true');
            } */
        }
    };
    
    // Generate infographic graphs
    
    $.fn.graph = function(options) {
        var settings = $.extend({
            'type': null,
            'dataX': null,
            'dataY': null,
            'dataZ': null,
            'dataArray': []
        }, options),

            target = this,
            shell = $('<div/>').attr('class','padding infographic'),
            percent = ((settings['dataX'] / settings['dataY']));

        // Circles

        if(settings.type==="Circle"){

            target.append(shell);
            

            var infographic = new ProgressBar.Circle(shell, {
                strokeWidth: 10,
                easing: 'easeInOut',
                duration: 1400,
                color: '#FFF',
                trailColor: '#FFF',
                trailWidth: .5,
                svgStyle: null,
                text: 'Test'
            });
            
            infographic.animate(percent);
        }
    };
    
    $.fn.tabList = function(goTo){
        
        var target = $(this),
            parent = target.parent(),
            menu = parent.find('ul.tab-menu'),
            tabs = target.children('li.tab'),
            menuButtons = [],
            labels = {};
        
        if(zz.checkVariable(goTo)){}else{
            goTo = 0;
        }
        
        for(var i = 0; i < tabs.length; i++){
            
            var item = $(tabs[i]),
                name = item.attr('data-name'),
                id = item.attr('data-asset'),
                li = $('<li class="inline-block"/>'),
                anchor = $('<a class="block tab-menu-button pointer bold no-underline default-untrim"/>').attr('href','#'),
                label = $('<div class="padding system"/>'),
                text = $('<span class="block"/>').attr({
                    'data-editable': 'true',
                    'data-attribute': 'name',
                    'data-save-to': id
                }).html(name);
            
            labels[i] = anchor;
            
            if(item.index()===goTo){
                item.show();
                anchor.addClass('default-trim');
                anchor.removeClass('default-untrim');
            }else{
                item.hide();
                anchor.removeClass('default-trim');
            }
            
            anchor.click(function(e){
                
                var thisIndex = $(this).parent('li').index(),
                    tab = tabs.eq(thisIndex);
                
                menu.find('.default-trim').removeClass('default-trim').addClass('default-untrim');
                
                $(labels[thisIndex]).removeClass('default-untrim').addClass('default-trim');
                
                tabs.hide();
                tab.show();
                
                e.preventDefault();
                
            });
            
            menuButtons.push(
                li.append(
                    anchor.append(
                        label.append(
                            text
                        )
                    )
                )
            );
        }
        
        menu.empty();
        menu.append(menuButtons);
    };
    
    $.fn.middleOfParent = function(){
        
        var middleOfParent = $('.always-middle-of-parent');
    
        for(var i = 0; i < middleOfParent.length; i++){
            var $target = $(middleOfParent[i]),
                $parent = $target.closest('.relative'),
                hidden = false;
            
            if($target.parent().hasClass('absolute')||$target.parent().hasClass('fixed')){
                $parent = $target.parent();
            }
            
            if($target.hasClass('hidden')){
                hidden = true;
                $target.removeClass('hidden');
            }

            var targetX = $target.outerWidth(),
                targetY = $target.outerHeight(),

                parentX = $parent.outerWidth(),
                parentY = $parent.outerHeight();

            $target.css({
                top: (parentY/2)-(targetY/2),
                left: (parentX/2)-(targetX/2)
            });
            
            if(hidden===true){
                $target.addClass('hidden');
            }
        }
    };
    
    $.fn.adjustGalleries = function(){
        
        var galleryItems = $('.gallery-item');
        
        for(var i = 0; i < galleryItems.length; i++){
            var target = $(galleryItems[i]),
                img = target.children('img'),
                width = img.attr('data-width'),
                height = img.attr('data-height'),
                X = img.outerWidth(),
                Y = img.outerHeight(),
                
                ratio = width/height,
                
                newWidth = Y*ratio;
            
            img.css('width',newWidth);
            
        }
    };
    
    $.fn.iconPattern = function(){
        $(this).find('.icon-tesselation').each(function(){
            var target = $(this),
                iconName= target.attr('data-icon'),

                targetWidth = target.outerWidth(),
                targetHeight = target.outerHeight(),

                horizCount = (targetWidth/15).toFixed(0),

                sizeClasses = [
                    'large-headline',
                    'headline',
                    'giant-headline',
                    'largerer',
                    'large',
                    'smaller'
                ],

                opacityClasses = [
                    'semi-transparent',
                    'almost-transparent',
                    'opaque'
                ],

                styleClasses = [
                    'black',
                    'untrim',
                    'stroke',
                    'white'
                ];

            target.addClass('center');

            for(var i = 0; i < 100; i++){
                var appendIconWrap = $('<div/>').attr('class','inline-block padding center'),
                    appendIcon = $('<i/>').attr('class','rotate-45 fa fa-'+iconName),

                    randomSize = sizeClasses[Math.floor((Math.random() * 5) + 0)],
                    randomOpacity = opacityClasses[Math.floor((Math.random() * 2) + 0)],
                    randomStyle = styleClasses[Math.floor((Math.random() * 3) + 0)];

                appendIconWrap.addClass(randomSize).addClass(randomOpacity).addClass(randomStyle);

                target.append(
                    appendIconWrap.append(
                        appendIcon
                    )
                );
            }
        });
    };
    
    $.fn.stickToHeader = function(){
        
        $('.stick-to-header.stuck').remove();
        $('.stick-to-header-placeholder').remove();

        var stickToHeaders = $('.stick-to-header');

        stickToHeaders.each(function(){

            var $target = $(this),
                scrollY = $target.attr('data-unstick'),
                height = $target.outerHeight(),
                clone;

            $(window).scroll(function(){

                var thisScroll = $(window).scrollTop();

                if(!scrollY){
                    scrollY = $target.offset().top;
                }

                if(thisScroll>=(scrollY-height)){

                    if(!$target.hasClass('stuck')){

                        clone = $('<div/>').attr('class','stick-to-header-placeholder').css('height',height);
                        
                        $target.after(clone);
                        $target.appendTo('.header-stickies');
                        $target.attr('data-unstick',scrollY);
                        $target.addClass('stuck');
                    }
                }else{
                    if($target.hasClass('stuck')){
                        $target.removeClass('stuck');
                        clone.after($target);
                        clone.remove();
                    }
                }

            });

        });
    };
    
    /* Animate content as user scrolls down page */
    
    $.fn.slideContentIn = function(loadInTimer,increment,element,threshOverride){
        
        // Disable load in animation on most tablets/phones
        if($(window).width()<=1024){
            $('.load-in-ani > *').css({
                'opacity': 1
            });
        }else{

            if(!threshOverride){
                threshOverride = 100;
            }

            var winHeight = $(window).height(),
                scroll = $(window).scrollTop(),
                thresh = winHeight+scroll-threshOverride,
                targets;

            if(!element||element===null||element.length<1){
                targets = $('.load-in-ani > *');
            }else{
                targets = element;
            }

            targets.each(function(){

                var target = $(this);

                target.css({'transition':'initial'}); 

                var currentBtm = $(this).css('bottom'),
                    currentHeight = $(this).outerHeight(),
                    offsetTop = $(this).offset().top,
                    preStoredBtm = $(this).attr('data-set-btm');

                if(!preStoredBtm||preStoredBtm===null||preStoredBtm===""){
                    preStoredBtm = currentBtm;
                    $(this).attr('data-set-btm',preStoredBtm);
                }

                if(!element||element===null||element.length<1){

                    if(target.hasClass('slided')){}else{
                        
                        if(offsetTop<scroll){
                            target.addClass('slided');
                            target.css({
                                'opacity': 1
                            });
                        }else{

                            if(offsetTop<thresh){

                                target.addClass('slided');

                                if(!preStoredBtm||preStoredBtm==="auto"||preStoredBtm===null||preStoredBtm==="initial"){
                                    preStoredBtm = 0;   
                                }

                                var targetPos = target.css('position');
                                if(!targetPos||targetPos===null||targetPos===""||targetPos==="static"){
                                    targetPos = "relative";   
                                }

                                target.css({
                                    'opacity': 0,
                                    'transform': 'translate3d(0px,50px,0px)',
                                    '-webkit-transform': 'translate3d(0px,50px,0px)',
                                    '-moz-transform': 'translate3d(0px,50px,0px)'
                                });

                                setTimeout(function(){
                                    target.css({
                                        'transition': '.8s cubic-bezier(0.075, 0.820, 0.165, 1.000)',
                                        'opacity': 1,
                                        'transform': 'translate3d(0px,'+preStoredBtm+',0px)',
                                        '-wbkit-transform': 'translate3d(0px,'+preStoredBtm+',0px)',
                                        '-moz-transform': 'translate3d(0px,'+preStoredBtm+',0px)'
                                    });
                                },loadInTimer);

                                loadInTimer = loadInTimer+increment;
                            }
                        }

                    }
                }else{

                    var targetPos = target.css('position');
                    if(!targetPos||targetPos===null||targetPos===""||targetPos==="static"){
                        targetPos = "relative";   
                    }

                    target.css({
                        'opacity': 0,
                        'transform': 'translate3d(0px,50px,0px)',
                        '-webkit-transform': 'translate3d(0px,50px,0px)',
                        '-moz-transform': 'translate3d(0px,50px,0px)'
                    });

                    setTimeout(function(){
                        target.css({
                            'transition': '.8s cubic-bezier(0.075, 0.820, 0.165, 1.000)',
                            'opacity': 1,
                            'transform': 'translate3d(0px,'+preStoredBtm+',0px)',
                            '-wbkit-transform': 'translate3d(0px,'+preStoredBtm+',0px)',
                            '-moz-transform': 'translate3d(0px,'+preStoredBtm+',0px)'
                        });

                        target.find('.typed').each(function(){
                            var text = $(this).text();
                            $(this).typed({
                                strings: [text],
                                typeSpeed: -5,
                                showCursor: false
                            });
                        });

                        setTimeout(function(){
                            target.css({'transition':'initial'}); 
                        },loadInTimer);

                    },loadInTimer);

                    loadInTimer = loadInTimer+increment;
                }
            });
            
        }
    };
    

})(jQuery);