$(document).ready(function(){
    
    $('.one-pager-wrapper').each(function(){
        var target = $(this);
        
        target.onePager('init');
        
        $(window).scroll();
        
        $(window).resize(function(){
            target.onePager('update');
        });
    });
    
    $('body').on('click','ul.one-pager-menu > li > a, .one-pager-editor-navigator > li > .listed-page-thumbnail',function(e){
        var item = $(this),
            index = item.parent('li').index(),
            pages = $('.one-pager-wrapper').children('.asset'),
            page = $(pages[index]),
            scroll = page.offset().top-$('header').outerHeight();
        
        $('html, body').stop(true,true).animate({
            scrollTop: scroll
        },1000,'easeOutQuart');
        
        e.preventDefault();
    });
    
    $('body').on('click','a.one-pager-next-link',function(e){
        var item = $(this).closest('.asset'),
            index = item.index()+1,
            pages = $('.one-pager-wrapper').children('.asset'),
            page = $(pages[index]),
            scroll = page.offset().top-$('header').outerHeight();
        
        $('html, body').stop(true,true).animate({
            scrollTop: scroll
        },1000,'easeOutQuart');
        
        e.preventDefault();
    });
    
});


(function($){
    
    var zz = $('<i/>'),
    
    // Non-repeating delay function
    typeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();

    function closestNumber(array,num){
        var i=0;
        var minDiff=1000;
        var ans;
        for(i in array){
             var m=Math.abs(num-array[i]);
             if(m<minDiff){ 
                    minDiff=m; 
                    ans=array[i]; 
                }
          }
        return ans;
    }
    
    $.fn.onePager = function(command){
        var target = $(this),
            pages,
            menuStyle;
        
        if(zz.checkVariable(target)){

            pages = target.children('.asset');
            
            menuStyle = target.attr('data-menu-style');
            
            if(zz.checkVariable(pages)){
                
                // INITIATE
                if(command==='init'){
                    
                    target.find('.generated-ui').remove();
                    $('.sticky-nav-tracks.one-pager-nav').empty();
                    
                    var asset = target.attr('data-asset'),
                        layout = target.attr('data-result-layout'),
                        nextTab = zz.element('div','absolute right bottom left large-padding center only-if-active'),
                        nextLink = zz.element('a','one-pager-next-link circle inline-block larger white no-underline').attr('href','#'),
                        nextIcon = zz.icon('angle-down'),
                        
                        // Menu elements
                        
                        pagesMenuWrap = zz.element('div','page-menu-wrap helper overflow-initial'),
                        pagesMenu = zz.element('ul','no-list list-menu no-wrap one-pager-menu').attr({
                            'role': 'tablist',
                            'data-asset': asset,
                            'data-result-layout': layout
                        }),
                        pagesMenuOptions = [];

                    nextTab.append(
                        nextLink.append(
                            nextIcon
                        )
                    );
                    
                    // Page generation

                    for(var i = 0; i < pages.length; i++){
                        
                        // Next page button
                        
                        if($(pages[i]).hasClass('window-height')){
                            $(pages[i]).matchWindowHeight();
                        }
                        
                        var thisNextTab = nextTab.clone();
                        
                        if(i<pages.length-1){
                        //    $(pages[i]).append(thisNextTab);
                        //    thisNextTab.introduce(1000,'bottom');
                        }
                        
                        // Menu links
                        
                        var count = i+1,
                            
                            name = $(pages[i]).attr('data-name'),
                            id = $(pages[i]).attr('data-asset'),
                            anchor = zz.element('a','block large no-underline short-transition relative').attr({
                                'id': 'page-tab-'+count,
                                'href': '#',
                                'role': 'tab',
                                'aria-controls': 'page-section-'+count
                            }),
                            icon = '', //zz.icon('caret-right'),
                            label = zz.element('span').attr({
                                "data-editable":"true",
                                "data-attribute":"short_name",
                                "data-save-to": id
                            }).html(name),
                            marker = zz.element('span','menu-active-item-marker absolute bottom right left margin-auto transition width-0'),
                            item = zz.element('li','list-menu-item').attr('data-asset',id).append(
                                anchor.append(
                                    icon
                                ).append(
                                    label
                                ).append(
                                    marker
                                )
                            );
                        
                        $(pages[i]).attr({
                            'id': 'page-section-'+count,
                            'aria-labelledby': 'page-tab-'+count,
                            'role': 'tabpanel'
                        });
                        
                        pagesMenuOptions.push(item);
                        
                        if(i===0){
                            // anchor.addClass('active untrim white-background');
                        }
                        
                        var nextPageLink = $(pages[i]).find('a.one-pager-next-link'),
                            nextPage = $(pages[i+1]);
                        
                        if(zz.checkVariable(nextPage)){
                            
                            var nextPageLabel = nextPage.attr('data-name'),
                                nextPageID = nextPage.attr('id');
                            
                            nextPageLink.children('span').html(nextPageLabel);
                            nextPageLink.show();
                            
                            nextPageLink.attr({
                                'aria-controls': 'page-section-'+(i+2)
                            });
                        }else{
                            nextPageLink.hide();
                        }
                    }
                    
                    pagesMenuWrap.append(
                        pagesMenu.append(
                            pagesMenuOptions
                        )
                    );
                    
                    if(menuStyle==="Always on Top"){
                        pagesMenuWrap.addClass('relative top').appendTo('.sticky-nav-tracks.one-pager-nav');
                        pagesMenu.children('li');
                    }
                    
                    var heightBuffer = zz.element('div','sticky-nav-height-buffer no-desktop relative top-layer white-background').css('height',pagesMenuWrap.outerHeight()-1);
                    // $(pages[0]).prepend(heightBuffer);
                    
                    var stuck = 0,
                        swiping = 1;
                    
                    $(window).scroll(function(){
                        
                        typeDelay(function(){
                            
                            var winScroll = $(window).scrollTop()+$('.header-height-buffer').outerHeight(),
                                offsets = [],
                                elements = [],
                                divs = {};
                            
                            if(winScroll>150){
                                $('.sticky-nav-tracks.one-pager-nav').addClass('opaque');
                            }else{
                                $('.sticky-nav-tracks.one-pager-nav').removeClass('opaque');
                            }

                            for(var i = 0; i < pages.length; i++){
                                var target = $(pages[i]),
                                    thisScroll = target.offset().top,
                                    thisLabel = target.attr('data-name'),
                                    alreadyActive = target.attr('data-slide-active');

                                offsets.push(thisScroll);
                                elements.push(target);
                                
                                divs[thisScroll] = {
                                    "label": thisLabel,
                                    "alreadyActive": alreadyActive,
                                    "element": target,
                                    "index": i
                                };

                            }

                            var closestOffset = closestNumber(offsets,winScroll),
                                closestElement = divs[closestOffset].label,
                                alreadyActive = divs[closestOffset].alreadyActive;

                            if(alreadyActive!=="1"){

                                for(var i = 0; i < pages.length; i++){
                                    $(pages[i]).attr({
                                        'data-slide-active':'0',
                                        'aria-hidden': 'true'
                                    });
                                    
                                    $(pages[i]).find('video.outline-shadow').each(function(){
                                        var vid = $(this);
                                        vid[0].pause();
                                    });
                                }
                                
                                pagesMenuWrap.swipeMenu(swiping,divs[closestOffset].index);
                                
                                // Highlight current item in editor nav (here to save multiple scroll listeners)
                                
                                if($('.one-pager-editor-navigator').length>0){
                                    
                                    var highlightThumb = $('.one-pager-editor-navigator').children('li').eq(divs[closestOffset].index),
                                        highlightThumbMarker = highlightThumb.find('.asset-marker');
                                    
                                    $('.one-pager-editor-navigator').find('.asset-marker').removeClass('opaque');
                                    highlightThumbMarker.addClass('opaque');
                                    
                                    scrollParent = highlightThumb.closest('.overflow-auto');
                                    
                                    // var scrollOffset = (highlightThumb.position().top-scrollParent.outerHeight(true))+(highlightThumb.outerHeight(true)*3.5);
                                    var scrollOffset = (highlightThumb.position().top)-15;
                                    
                                    console.log(scrollOffset);
                                    
                                    if(divs[closestOffset].index===0){
                                        scrollOffset = 0;
                                    }
                                    
                                    scrollParent.stop(true,true).animate({
                                        scrollTop: scrollOffset
                                    },500,'easeOutExpo');
                                }
                                
                                divs[closestOffset].element.find('video.outline-shadow').each(function(){
                                    var vid = $(this);
                                    vid[0].play();
                                });
                                
                                divs[closestOffset].element.attr({
                                    'data-slide-active':'1',
                                    'aria-hidden': 'false'
                                });
                            }
                            
                        },100);
                    });
                }
                    
                if(swiping===1){
                    swiping = pagesMenuWrap.swipeMenu(null,null,1);
                }
                
                // UPDATE
                if(command==='update'){
                    for(var i = 0; i < pages.length; i++){
                        
                        if($(pages[i]).hasClass('window-height')){
                            $(pages[i]).css('margin-top','-1px').matchWindowHeight();
                        }
                    }
                }                
            }
        }
    };

})(jQuery);