/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    plumber = require('gulp-plumber');
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    //jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');
    browserSync = require('browser-sync').create();
    useref = require('gulp-useref');
    runSequence = require('run-sequence');
    gulpIf = require('gulp-if');
    rename = require("gulp-rename");
    fileinclude = require('gulp-file-include'),
  

gulp.task('fileinclude', function() {
  gulp.src(['src/index.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('public/'));
});


//script paths
var jsFiles = 'localDev/javascript/**/*.js',  
    jsDeploy = 'bitbucket/minified/javascript';
    jsBuild = 'bitbucket/unminified/javascript';


gulp.task('scriptsDev', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDev))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDeploy));
});

gulp.task('scriptsDeploy', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsBuild))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDeploy));
});



var cssLocal = 'localDev/styles/**/*.css';

// Styles
gulp.task('sass', function() {
  return sass(cssLocal)
    //.pipe(autoprefixer('last 2 version'))
    .pipe(plumber())
    .pipe(gulp.dest('bitbucket/unminified/stylesheets/'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('bitbucket/minified/stylesheets/'))
    //.pipe(browserSync.reload ({
    //  stream: true
   // })
    ;
});

gulp.task('cssbuild', function() {
  return gulp.src(cssLocal)
    .pipe(gulp.dest('bitbucket/unminified/stylesheets/'))
    .pipe(cssnano())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('bitbucket/minified/stylesheets/'))
})
// Scripts
gulp.task('scripts', function() {
  return gulp.src('src/javascripts/modules/**/*.js')
    //.pipe(jshint('.jshintrc'))
    //.pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('public/scripts'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('public/scripts'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('public/images'))
    .pipe(notify({ message: 'Images task complete' }));
});


gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'src'
    },

  })
})

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./src"
    });
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('cssbuild', 'scriptsbuild');
});

gulp.task('build', function (callback) {
  runSequence(['cssbuild','scriptsbuild'],
    callback
  )
})

// Watch
gulp.task('watch', ['browserSync', 'cssbuild', 'scriptsbuild'],function() {
  // Watch .scss files
  gulp.watch('localDev/styles/**/*.css', ['cssbuild']);
  gulp.watch('locaDevl/javascripts/**/*.js', browserSync.reload);
  //gulp.watch('src/*.html', browserSync.reload);
  
});






